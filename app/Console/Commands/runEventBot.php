<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Pressutto\LaravelSlack\Slack;
use App\Event;
use Notification;
use App\Notifications\NewEventNotification;

class runEventBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:eventbot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check new events from lanacion black';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $endpoint = "https://experienciasblack.lanacion.com.ar/";
        $client = new \GuzzleHttp\Client();
        $cookie = env( 'COOKIE' );
        
        $response = $client->request('GET', $endpoint, ['headers' => [ 'cookie' => $cookie, 'content-type' => 'text/html; charset=UTF-8' ] ] );

        if ( $response->getStatusCode() == 200 ) {

            // Get Content
            $content = (string) $response->getBody();
            
            // Casting HTML content
            $html = mb_convert_encoding( $content, 'UTF-8', 'UTF-8');

            // Events
            preg_match_all( '/data-condition=\\"(today|coming|outofstock)\\">.+<figcaption>Reservá ahora<\/figcaption>.+<h2><a href="\/experiencia\/([0-9]++)">(.+)<.+<\/div>/Us', $html, $events );
            
            // Update or Create Event and get new ones in array list.
            $newEvents = $this->updateOrCreateEvent( $events );

            for( $i = 0; $i < count( $newEvents ); $i++ ) {
                Notification::route('slack', env( 'SLACK_WEBHOOK_URL' ) )
                            ->notify( new NewEventNotification( $newEvents[ $i ] ) );
            }

            // Soft Delete events where are not displayed anymore.
            Event::whereNotIn( 'id', $events[ 2 ] )->delete();
        }
    }

    private function updateOrCreateEvent( $events ) {
        $newEvents = array();
        for( $i=0; $i < count( $events[ 2 ] ); $i++ ) {
            $event = Event::where( 'id', $events[ 2 ][ $i ] )->withTrashed()->first();
            if ( !$event ) {
                if ( $events[ 1 ][ $i ] == 'today' ) {
                    $newEvents[] = [
                        'id' => $events[ 2 ][ $i ],
                        'title' => $events[ 3 ][ $i ],
                        'state' => $events[ 1 ][ $i ],
                    ];
                }
                $event = new Event();
            }

            $event->id = $events[ 2 ][ $i ];
            $event->title = $events[ 3 ][ $i ];
            $event->state = $events[ 1 ][ $i ];
            $event->deleted_at = null;
            $event->save();
        }
        return $newEvents;
    }
}
