<h2>Hoy</h2>
@foreach ( $today as $event )
    <p><a target="_blank" href="https://experienciasblack.lanacion.com.ar/experiencia/{{ $event->id }}">{{ $event->id }}</a> - <b>{{ $event->title }}</b></p>
@endforeach
<h2>Próximamente</h2>
@foreach ( $coming as $event )
    <p><a target="_blank" href="https://experienciasblack.lanacion.com.ar/experiencia/{{ $event->id }}">{{ $event->id }}</a> - <b>{{ $event->title }}</b></p>
@endforeach
<h2>Agotadas</h2>
@foreach ( $outofstock as $event )
    <p><a target="_blank" href="https://experienciasblack.lanacion.com.ar/experiencia/{{ $event->id }}">{{ $event->id }}</a> - <b>{{ $event->title }}</b></p>
@endforeach