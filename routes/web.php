<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    Artisan::call( 'run:eventbot' );
    return view( 'events', [ 
        'today' => \App\Event::where( 'state', 'today' )->orderBy( 'created_at', 'desc' )->get(),
        'coming' => \App\Event::where( 'state', 'coming' )->orderBy( 'created_at', 'desc' )->get(),
        'outofstock' =>\App\Event::where( 'state', 'outofstock' )->orderBy( 'created_at', 'desc' )->get()
     ]);
});
